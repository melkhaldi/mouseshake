
import java.util.Optional;
import javafx.application.Application;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import org.reactfx.EventStream;
import org.reactfx.EventStreams;
import org.reactfx.StateMachine;
import org.reactfx.util.Tuple2;
import static org.reactfx.util.Tuples.*;

public class MouseShakeDemo extends Application {

    private static enum Dir {
        LEFT, RIGHT
    }

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) {
        Pane pane = new Pane();

        shakes(pane, 4, 100, 200).subscribe(shake -> System.out.println("SHAKE!!!"));

        stage.setScene(new Scene(pane, 400, 400));
        stage.show();
    }

    /**
     * Emits an event when a mouse shake occurs on the given node.
     *
     * @param node
     * @param n The number of mouse turns that need to occur before recognize
     * the gesture. Must be at least 2.
     * @param maxDist maximum distance, in pixels, that the mouse has to travel
     * between turns
     * @param maxDelay maximum delay, in milliseconds, between turns
     */
    private EventStream<?> shakes(Node node, int n, double maxDist, long maxDelay) {
        // we're going to track the number of turns and the last turn
        // as the state of a state machine
        return StateMachine.init(t(0, (Tuple2<Double, Long>) null)) // start with no turns
                .on(mouseTurns(node))
                .transmit((state, turn) -> state.<Tuple2<Tuple2<Integer, Tuple2<Double, Long>>, Optional<Object>>>map((cnt, lastTurn) -> {
                    if (lastTurn == null) {
                        // track the first turn, don't emit anything
                        return t(t(1, turn), Optional.empty());
                    } else {
                        double dist = Math.abs(turn._1 - lastTurn._1);
                        long delay = turn._2 - lastTurn._2;
                        if (dist <= maxDist && delay <= maxDelay) { // we are within the allowed delay and distance
                            if (cnt + 1 == n) { // reached the required number of turns
                                // emit an event and start counting from 0
                                return t(t(0, null), Optional.of(turn));
                            } else {
                                // increase the counter, but don't emit yet
                                return t(t(cnt + 1, turn), Optional.empty());
                            }
                        } else { // too much delay or too big distance, start counting from one
                            return t(t(1, turn), Optional.empty());
                        }
                    }
                }))
                .toEventStream();
    }

    // emits x-coordinate and timestamp (in milliseconds) of mouse turns
    private EventStream<Tuple2<Double, Long>> mouseTurns(Node node) {
        // we're going to track last mouse position and direction
        // as the state of a state machine
        return StateMachine.init(t((Double) null, (Dir) null)) // start with no value
                .on(EventStreams.eventsOf(node, MouseEvent.MOUSE_DRAGGED))
                .transmit((state, evt) -> state.<Tuple2<Tuple2<Double, Dir>, Optional<Tuple2<Double, Long>>>>map((lastX, lastDir) -> {
                    if (lastX == null) {
                        // start recording the x position. Can't determine direction yet.
                        return t(t(evt.getX(), (Dir) null), Optional.empty());
                    } else {
                        Dir dir = evt.getX() - lastX > 0 ? Dir.RIGHT : Dir.LEFT;
                        if (lastDir == null || lastDir == dir) { // direction unchanged
                            // record the new position and direction, don't emit anything
                            return t(t(evt.getX(), dir), Optional.empty());
                        } else {
                            // record the new position and direction and emit the turn
                            return t(t(evt.getX(), dir), Optional.of(t(lastX, System.currentTimeMillis())));
                        }
                    }
                }))
                .toEventStream();
    }

}
